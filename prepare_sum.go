package main

import (
	"math"
	"strconv"
	"strings"
)

// prepareSum возвращает итоговую сумму готовую для слияния
func prepareSum(invertSumStr string, delimiter string) string {
	sumStr := revert(invertSumStr)

	sumRubles, err := strconv.Atoi(sumStr)
	if err != nil {
		return ""
	}

	sumCurr := conversion(sumRubles, CurrencyRate)
	separatedSumStr := separateBigNumber(sumCurr, delimiter)

	return separatedSumStr
}

// revert - переворачивает сумму, потому что она формируется в обратном порядке в цикле с конца слайса
func revert(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}

	return string(runes)
}

// conversion - конвертация в нужную валюту и округление в большую сторону
func conversion(sum int, currency float64) string {
	result := math.Ceil(float64(sum) / currency)
	res := strconv.Itoa(int(result))

	return res
}

// separateBigNumber -разделяет число через каждые три знака начиная с конца
func separateBigNumber(numStr string, sep string) string {
	parts := make([]string, 0, (len(numStr)+2)/3)
	for len(numStr) > 3 {
		parts = append(parts, numStr[len(numStr)-3:])
		numStr = numStr[:len(numStr)-3]
	}
	if len(numStr) > 0 {
		parts = append(parts, numStr)
	}

	return strings.Join(reverse(parts), sep)
}

func reverse(s []string) []string {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}

	return s
}
