package main

const (
	IndivisibleSpace = "&nbsp;"
	CurrencyRate     = 0.21
	CurrencyName     = "Тенге"
)

var RublesAliases = []string{
	"рублей",
	"&#8381;",
	"руб.",
}
