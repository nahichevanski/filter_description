package main

import "strings"

// checkSubSliceDescription обрабатывает
func checkSubSliceDescription(description string, rubAliasDelim string) string {

	//пытаемся разбить описание на части по одному из вариантов названия рублей
	sliceDescription := strings.Split(description, rubAliasDelim)

	//если такой вариант в тексте отсутствует
	//возвращаем исходное описание для дальнейшей обработки
	if len(sliceDescription) < 2 {
		return description
	}

	sb := strings.Builder{}

	//если такой вариант в тексте есть то в цикле
	//обрабатываем каждую часть описания
	for _, v := range sliceDescription {
		price, index := extractSumAndIndex(v)
		if index == 0 {
			sb.WriteString(v)
			break
		}

		sb.WriteString(v[:len(v)-index])
		sb.WriteByte(' ')
		sb.WriteString(price)
		sb.WriteByte(' ')
		sb.WriteString(CurrencyName)
	}

	return sb.String()
}
