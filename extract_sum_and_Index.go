package main

import (
	"strings"
	"unicode"
)

func extractSumAndIndex(partDesc string) (string, int) {
	sb := strings.Builder{}

	//еслистрока пустая то сразу выходим
	if partDesc == "" {
		return sb.String(), 0
	}

	partDescriptions := []rune(partDesc)
	delimiter := " "
	count := 0

	//у каждого начиная с конца извлекаем сумму и индекс с которого начинается эта сумма
	for i := len(partDescriptions) - 1; i >= 0; i-- {
		if unicode.IsDigit(partDescriptions[i]) {
			sb.WriteRune(partDescriptions[i])
			count++
			continue
		}
		if partDescriptions[i] == ' ' {
			count++
			continue
		}
		if partDescriptions[i] == ';' && count != 0 {
			size := len(IndivisibleSpace) - 1
			sus := partDescriptions[i-size : i+1]
			if checkDelimiter(sus, &count) {
				delimiter = IndivisibleSpace
				i = i - size
			} else {
				break
			}
		} else {
			break
		}
	}

	result := prepareSum(sb.String(), delimiter)

	return result, count
}

// checkDelimiter есть неделимый пробел, если совпадает то по указателю переключает счетчик
func checkDelimiter(text []rune, count *int) bool {
	c := *count

	for i, v := range text {
		if v != rune(IndivisibleSpace[i]) {
			*count = c
			return false
		}
		*count++
	}

	return true
}
