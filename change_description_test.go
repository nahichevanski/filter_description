package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestChangeDescription_Fail(t *testing.T) {
	tests := []struct {
		description       string
		resultDescription string
	}{
		{
			description:       "test",
			resultDescription: "test",
		},
		// непредвиденный разделитель сломает программу
		//{
		//	description:       "от 10s;&bpn000 руб.",
		//	resultDescription: "от 10s;&bpn000 руб.",
		//},
		// такой разделитель тоже сломает программу
		//{
		//	description:       "от 10000 tруб.",
		//	resultDescription: "от 10000 tруб.",
		//},
	}

	for _, test := range tests {
		res := ChangeDescription(test.description)
		assert.Equal(t, test.resultDescription, res)
	}
}

func TestChangeDescription(t *testing.T) {
	tests := []struct {
		description       string
		resultDescription string
	}{
		{
			description:       `«Опт «Праздничное освещение»» превышает 10&nbsp;000 рублей, дополнительная скидка`,
			resultDescription: `«Опт «Праздничное освещение»» превышает 47&nbsp;620 Тенге, дополнительная скидка`,
		},
		{
			description:       `u003EПри заказах от 1 000 000 руб. свяжитесь с менеджером д`,
			resultDescription: `u003EПри заказах от 4 761 905 Тенге свяжитесь с менеджером д`,
		},
		{
			description:       `аров / Скидка</h4><ul>  <li>От 50 000 &#8381; /&nbsp;`,
			resultDescription: `аров / Скидка</h4><ul>  <li>От 238 096 Тенге /&nbsp;`,
		},
		{
			description:       `«Опт «Праздничное освещение»» превышает 10&nbsp;000 рублей, дополнительная / Скидка</h4><ul>  <li>От 50 000 &#8381; /&nbsp;`,
			resultDescription: `«Опт «Праздничное освещение»» превышает 47&nbsp;620 Тенге, дополнительная / Скидка</h4><ul>  <li>От 238 096 Тенге /&nbsp;`,
		},
		{
			description:       `«Опт «Праздничное освещение»» превышает от 1 000 000 руб. свяжитесь дополнительная / Скидка</h4><ul>  <li>От 50 000 &#8381; /&nbsp;`,
			resultDescription: `«Опт «Праздничное освещение»» превышает от 4 761 905 Тенге свяжитесь дополнительная / Скидка</h4><ul>  <li>От 238 096 Тенге /&nbsp;`,
		},
		{
			description:       `превышает 10&nbsp;000 рублей, дополнительная При заказах от 1 000 000 руб. свяжитесь с`,
			resultDescription: `превышает 47&nbsp;620 Тенге, дополнительная При заказах от 4 761 905 Тенге свяжитесь с`,
		},
	}

	for _, test := range tests {
		res := ChangeDescription(test.description)
		assert.Equal(t, test.resultDescription, res)
	}
}

func BenchmarkChangeDescription(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = ChangeDescription(text)
	}
}

func BenchmarkReplaceCurrency(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = ReplaceCurrency(text, *rgxp)
	}
}
